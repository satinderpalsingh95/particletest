//
//  ViewController.swift
//  ParticleTest
//
//  Created by Satinder pal Singh on 2019-11-07.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    let USERNAME = "satinderpalsingh95@gmail.com"
    let PASSWORD = "Satti1234"
    
    let DEVICE_ID = "38001b001047363333343437"
    var myPhoton : ParticleDevice?
    var timer = 0;
    var counterTimer = Timer()
    var speed = 100;
    var maxTimer = 20;
    var sliderValue = 1
    @IBOutlet weak var timeSlowsDownLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var kianiLabel: UILabel!
    
    func startCounterTimer() {
        
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(decrementCounter), userInfo: nil, repeats: true)
        
        
    }
    @objc func decrementCounter(){
        
        if(timer == 0 ){
            showEmotions(num: "\(speed)")
            timer += 1
            
            timeLabel.text = "\(timer)"
        }
        else if (timer >= 1 && timer < maxTimer){
            
            timer += 1
            
            timeLabel.text = "\(timer)"
        }
        else{
            timer = -1;
            timeLabel.text = "0"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
            }
                
            }
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                //self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: "playerChoice", handler: {
            (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event?.data)")
                let choice = (event?.data)!
                
            
            }
        })
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        //timer started
        if timer == 0
        {
            timer = 0;
            startCounterTimer()
        }
        else
        {
            timer = 0;
        }
        
        
        
        
    }
    
    @IBAction func kianiSliderChanged(_ sender: UISlider) {
       
        sliderValue = Int(sender.value)
         kianiLabel.text = "Amount Of Kiani:- \(sliderValue)"
        //speed = speed * currentValue!
        //maxTimer = maxTimer * currentValue
    }
    
    func showEmotions(num: String) {
        print("show Emotions called")
        
        speed = 100 * sliderValue
        maxTimer = 20 * sliderValue
        timeSlowsDownLabel.text = "Time Slows down by:- \(maxTimer-20)sec"
        let parameters = ["\(speed)"]
        var task = myPhoton!.callFunction("showEmotions", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
    }
    


}


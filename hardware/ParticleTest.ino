// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton b = InternetButton();

void setup() {
b.begin();
Particle.function("showEmotions", showEmotions);

}

void loop() {


}

int showEmotions(String num)
{
Particle.publish("showEmotions called",num,60,PRIVATE);
int num1 = num.toInt();

b.ledOn(1, 255,255,255);
b.ledOn(11, 255,255,255);
b.ledOn(4, 255,255,255);
b.ledOn(5, 255,255,255);
b.ledOn(6, 255,255,255);
b.ledOn(7, 255,255,255);
b.ledOn(8, 255,255,255);
delay(50*num1);
//delay(300);
//b.allLedsOff();


b.ledOff(4);
b.ledOff(8);
delay(50*num1);

//delay(300);
//b.allLedsOff();


b.ledOff(5);
b.ledOff(7);
delay(50*num1);
//delay(300);
//b.allLedsOff();

b.allLedsOff();
b.ledOn(1, 255,0,0);
b.ledOn(11, 255,0,0);
//b.ledOn(4, 255,0,0);
b.ledOn(5, 255,0,0);
b.ledOn(6, 255,0,0);
b.ledOn(7, 255,0,0);
//b.ledOn(8, 255,0,0);
delay(50*num1);

b.allLedsOff();



return 0;
}




